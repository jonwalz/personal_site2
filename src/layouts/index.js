import React from "react"
import { Container, Row } from "reactstrap"
import PropTypes from "prop-types"
import graphql from "graphql"
import Link from "gatsby-link"
import Helmet from "react-helmet"
import styled from "styled-components"
import Drawer from "@material-ui/core/Drawer"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
import ListItem from '@material-ui/core/ListItem';
import { Footer } from "../components/footer"
import "prismjs/themes/prism-twilight.css"

// main site style
import "./index.scss"

class TemplateWrapper extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            width: 0,
            height: 0,
            isOpen: false,
        }

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
        this.toggleDrawer = this.toggleDrawer.bind(this)
    }

    toggleDrawer(isOpen) {
        this.setState({
            isOpen,
        })
    }
    componentDidMount() {
        this.updateWindowDimensions()
        window.addEventListener("resize", this.updateWindowDimensions)
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindowDimensions)
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight })
    }

    withDrawer(child) {
        return (
            <Drawer
                open={this.state.isOpen}
                onClose={this.toggleDrawer.bind(this, false)}
                className="side-drawer"
            >
                {child}
            </Drawer>
        )
    }

    content() {
        return (
            <Container>
                <Link to="/" className="navbar-brand">
                    {this.props.data.site.siteMetadata.title}
                </Link>
                <Ul className="nav navbar-nav">
                    <li className="nav-item">
                        <Link to="/blog_list" className="nav-link">
                            Blog
                        </Link>
                    </li>

                    <li className="nav-item">
                        <Link to="/portfolio" className="nav-link">
                            Portfolio
                        </Link>
                    </li>

                    <li className="nav-item">
                        <Link to="/contact" className="nav-link">
                            Contact
                        </Link>
                    </li>
                </Ul>
            </Container>
        )
    }

    render() {
        const { children, data } = this.props
        const isMobile = this.state.width < 625
        let user
        if (typeof window !== "undefined") {
            user =
                window.netlifyIdentity && window.netlifyIdentity.currentUser()
        }
        return (
            <div className="App">
                <Helmet title={data.site.siteMetadata.title} />
                <Navbar className="navbar navbar-expand-lg navbar-dark">
                    {isMobile && (
                        <IconButton
                            color="inherit"
                            onClick={this.toggleDrawer.bind(this, true)}
                        >
                            <MenuIcon />
                        </IconButton>
                    )}
                    {isMobile ? (
                        this.withDrawer(this.content())
                    ) : (
                        this.content()
                    )}
                </Navbar>
                <div className="pageContent">{children()}</div>
                <Footer />
            </div>
        )
    }
}

TemplateWrapper.propTypes = {
    children: PropTypes.func,
}

export const pageQuery = graphql`
    query LayoutIndexQuery {
        site {
            siteMetadata {
                title
            }
        }
    }
`

export default TemplateWrapper

const Ul = styled.ul`
    li:last-of-type {
        margin-right: 0;

        a {
            padding-right: 0;
        }
    }
`

const Navbar = styled.div`
    background-color: #2f577f;
    ul {
        display: flex;
        flex-direction: row;
    }
    .side-drawer {
        background-color: #2f577f;
        color: white;
    }

    button:focus {
      outline: none;
    }
    svg {
      fill: #fff !important;
    }

    @media (max-width: 620px) {
        .navbar-nav {
            flex-direction: column !important;
            border: 3px solid burlywood;
        }
        ul {
            text-align: center;
            color: pink;

            .nav-item {
                margin: 0;
            }
        }
    }
`
