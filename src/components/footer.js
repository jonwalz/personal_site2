import React from 'react'
import styled from "styled-components"
import { Row } from 'reactstrap';

export const Footer = () => {
  return (
    <StyledRow>
      <span>2018</span>
      <div>
        <a href="https://www.github.com/jonwalz" target="_blank">Github</a>
        <a href="https://www.linkedin.com/in/jonathanwalz" target="_blank">LinkedIn</a>
      </div>
    </StyledRow>
  )
}

const StyledRow = styled(Row)`
  margin: 0 !important;
  width: 100%;
  background-color: #2F577F;
  color: white;
  display: flex;
  justify-content: space-between;
  padding: 0 2.2em;

  a, a:active, a:visited {
    color: white;
    margin: 0 5px;
  }

  a:hover {
    color: #C7D2FF;
    text-decoration: none;
  }
`