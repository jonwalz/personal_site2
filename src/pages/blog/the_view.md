---
contentType: blog
path: /taking-the-leap
title: "Taking the Leap"
date: 2018-11-06T12:25:53-07:00
---
I love learning. The strange part of this realization is that I did terrible in school. I’m honestly surprised they gave me a high school diploma. I don’t know what the criteria was back then but if I was suppose to maintain a c average to graduate, well, I don’t know why I graduated. My story is similar to Gary Vaynerchuck (aside from owning a massively successful marketing agency, I still consider myself successful). What’s the point of this? Well because of doing really not great in the arena of academia, I’ve always carried this doubt of my own intelligence. And this doubt actually fuels my desire and to learn and as a result, I’ve been able to achieve is all the goals I have set for myself.

In my 20’s, I dreamed of being a coffee professional. This may sound strange to those who have aspired to high profile, high salary jobs. I believe this dream came as a result of growing up in the northwest where coffee is taken very seriously and something about the counterculture of it appealed to my non conformist nature. Well after committing myself to years of study (through field research, aka barista) I landed a job with one the largest coffee companies in the world. In this role I roasted and help select the coffee to be served in their flagship specialty storefront. This coffee dream lasted for roughly 7 years when a new dream goal was setting in my mind: I wanted to program computers. This sounds like an odd left turn in my career life but I had actually been tinkering with computers since I was 13 at this point. Most of this tinkering was done in the form of html and little bit of command line. So naturally I made my first career goal to be web development.

It’s now the two year mark being employed as a developer. I’ve mostly been working on web applications with a sprinkle of mobile (in the form of react native) probably because that’s what I’m good at/enjoy. This experience has opened such a wide variety of possibly directions to take, I’ve decided to make a deliberate goal for the next direction of my career. This goal actually spawns from a few different interests in my life pointing to the same technology: c++.

Why c++ might you ask? Well here’s the thing. I’ve always wanted to make a software synthesizer and possibly a web version of one. There's a great tool for this job called [JUCE](https://juce.com/). The possibilities with web assembly are quite attractive too. I’ve also wanted to learn to develop for VR and many of those platforms provide APIS in c/c++. We recently bought a Magic Leap at the office and I am excited to develop for that. Anyhow, that's my rant for today.


