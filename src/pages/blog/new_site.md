---
contentType: blog
path: /new-site
title: So I finnaly made a new site
date: 2018-09-16T12:25:53-07:00
---

I finally got around to this site. I used it as an opportunity to learn [Gatsbyjs](https://www.gatsbyjs.org/), a static site generator. Highly recommended by the way!

If you're wanting to know more about my experience as an engineer, I can briefly sum that up here. I started as an intern at Olio Apps in November of 2016. I was immediately thrown into creating components with react and learning their stack and best practices. The experience was really great and I am very grateful for the guys I work for. They are very knowledgable and understand the importance of phycological security in the software development environment.

After interning for 2 months, I had the opportunity to do some contract work for  about 3 months building a product with their React and Redux stack and honing my skills as a developer. I have now been with the company for almost 2 years, which will be in November. I have used many different react related javascript libraries such as Redux-Saga, React-Router, Css-Modules, Styled-Components, Axios, Jest, Mocha, Typescript, React-Spring, React-Motion, Redux-Saga-Test-Plan... the list can keep going, really.

Lately I've played a larger role in researching and presenting technologies to be used in future projects. This included finding a reliable way to test our Redix-Saga generator functions. Also, finding solutions for end-to-end testing of a client project and educating others on the team of how to use it.

One the most gratifying aspects of this position is having the opportunity to mentor the new developers and help guide them in their process of learning. I've also been mentoring in my free time and finding this some of the most inspirational experiences of this kind of work. Seeing that light bulb moment in someone is an amazing and enlightening experience.

So this was just a brief status update of my career life for whoever happens upon this! I'll probbaly post more things specific to coding, such as tutorials soon. Also, possibly a way for me to keep track of my own progress ;)
