import React from 'react'
import { Container, Card, CardText, CardBody, CardTitle, CardSubtitle, Jumbotron } from 'reactstrap'
import styled from "styled-components"
import { Transition } from "react-spring"
import Link from 'gatsby-link'
import graphql from 'graphql'

const BloglistPage = ({ data }) => {
    const { allMarkdownRemark: { edges }} = data
    const blogs = edges.filter(post => post.node.frontmatter.contentType === 'blog')
    return (
        <Transition
            force
            from={{ opacity: 0 }}
            enter={{ opacity: 1 }}
            leave={{ opacity: 0 }}
        >
            {style => (
                <Container style={style}>
                    {blogs.map(({ node: post }) => (
                        <Card style={{marginBottom: 10}} key={post.id}>
                            <CardBody>
                            <CardTitle><Link to={post.frontmatter.path}>{post.frontmatter.title}</Link></CardTitle>
                            <CardSubtitle style={{marginBottom: 10}}>{post.frontmatter.date}</CardSubtitle>
                            <CardText>{post.excerpt}</CardText>
                            </CardBody>
                        </Card>
                    ))}
                </Container>
            )}
        </Transition>
    )
}

export default BloglistPage

export const pageQuery = graphql`
  query BloglistQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          frontmatter {
            title
            contentType
            date(formatString: "MMMM DD, YYYY")
            path
          }
        }
      }
    }
  }
`

const StyledH1 = styled.h1`
  background-color: pink;
  padding: 10px;
`