import React from 'react'
import {
	Fade,
	Container,
	Card,
	CardText,
	CardBody,
	CardTitle,
	CardSubtitle,
	Jumbotron,
} from 'reactstrap'
import styled from 'styled-components'
import Link from 'gatsby-link'
import { Transition } from 'react-spring'
import graphql from 'graphql'
import 'bootstrap/dist/css/bootstrap.min.css'
const ProfilePic = require('../../static/files/profile.jpg')

const IndexPage = () => (
	<Transition
		force
		from={{ opacity: 0 }}
		enter={{ opacity: 1 }}
		leave={{ opacity: 0 }}
	>
		{(style) => (
			<Container>
				<StyledJumbotron fluid>
					<section style={style}>
						<main>
							<StyledH1>Hi, I'm Jon Walz</StyledH1>
							<p>
								I'm a developer currently residing in the land of Portland,
								Oregon. <br />
								Since November of 2016, I've spent most of my working day
								developing web applications with React, Redux, Typescript and
								various css libraries and preprocessors.
							</p>
						</main>
						<img src={ProfilePic} alt="Profile Picture" />
					</section>
				</StyledJumbotron>
			</Container>
		)}
	</Transition>
)

export default IndexPage

export const pageQuery = graphql`
	query IndexQuery {
		allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
			edges {
				node {
					excerpt(pruneLength: 400)
					id
					frontmatter {
						title
						contentType
						date(formatString: "MMMM DD, YYYY")
						path
					}
				}
			}
		}
	}
`

const StyledJumbotron = styled(Jumbotron)`
  background-color: transparent !important;
  margin-top: 20px;

  section {
    display: grid;
    grid-template-columns: auto min-content;
    padding: 0 15px;

    main {
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding-right: 20px;
    }

    img {
      width: 100%;
      max-width: 300px;
      justify-self: end;
      border-radius: 50%;
      border: 5px solid #2F577F;
      min-height: 260px;
      min-width: 260px;
      align-self: center;
      justify-self: center;
    }

    p {
      line-height: 24px;
      font-weight: 200;
    }
  }
  @media all and (max-width: 530px) {
    section {
        grid-template-columns: 100%;
        grid-row-gap: 15px;
    }

    img {
        grid-row-start: 1;
    }
    main {
        grid-row-start: 2;
    }
`
const StyledH1 = styled.h1`padding: 10px 0s;`
const Section = styled.section`margin: 10px 0;`
