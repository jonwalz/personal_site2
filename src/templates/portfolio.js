import React from 'react'
import { CardImg, Card, CardTitle, CardBody, Container, Col, Row } from 'reactstrap'
import { Transition } from 'react-spring'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import graphql from 'graphql'
import { Trail, animated } from 'react-spring'
import { projects } from '../portfolio/data.json'
import { openInWindow } from '../util/portfolio_util'

const ProjectImageContainer = styled.div`
	text-align: center;
	width: 100%;
	cursor: pointer;

	img {
		max-width: 100%;
	}
`

const ProjectImage = ({ imageUrl, projectUrl }) => {
	return (
		<ProjectImageContainer>
			<CardImg top src={imageUrl} alt="Project Image" onClick={openInWindow.bind(this, projectUrl)} />
		</ProjectImageContainer>
	)
}

const Placeholder = styled.div`
	width: 100%;
	height: 100%;
	background-color: #2f577f;
`

const Title = ({ projectName, projectUrl }) => {
	return (
		<CardTitle onClick={openInWindow.bind(this, projectUrl)} className="project-title">
			{projectName}
		</CardTitle>
	)
}

const TitleStyled = styled(Title)`
  cursor: pointer;
`

const StyledRow = styled.div`
	display: grid;
	flex-direction: row;
	grid-template-columns: 1fr 1fr;
	grid-column-gap: 10px;
	grid-row-gap: 10px;

	.col {
		grid-column-start: 1;
		display: flex;
		align-items: center;
	}

	.description {
		grid-column-start: 2;
		flex-direction: column;
		text-align: center;
	}

	.image-wrapper {
		display: flex;
		justify-content: center;
		align-items: center;
		box-shadow: 0 0 1px rgba(0, 0, 0, 0.5);
		border-radius: 4px;
		background-color: rgba(0, 0, 0, 0.03);
		padding: 10px;
	}

  .project-title {
    cursor: pointer;
    font-weight: 600;
  }
	@media (max-width: 600px) {
		grid-template-columns: 1fr;
		.col {
			grid-row-start: 1;
			grid-column-start: 1;
		}
		.description {
			grid-row-start: 2;
			grid-column-start: 1;
		}
	}
`

export default function Template({ data }) {
	const { markdownRemark: post } = data

	return (
		<Transition force from={{ opacity: 0 }} enter={{ opacity: 1 }} leave={{ opacity: 0 }}>
			{(style) => (
				<div style={style}>
					<Helmet title={`${post.frontmatter.title} | ${data.site.siteMetadata.title}`} />
					<Container className="portfolio-container">
						<h3 className="display-4">{post.frontmatter.title}</h3>
						<Trail
							native
							from={{
								opacity: 0,
								x: 30,
							}}
							to={{
								opacity: 1,
								x: 0,
							}}
							keys={projects.map((project) => project.id)}
						>
							{projects.map((project) => (styles) => {
								const { opacity, x } = styles
								const imageUrl = project.image_url.length > 0
								return (
									<animated.div
										style={{
											opacity,
											transform: x.interpolate((x) => `translate3d(${x}%,0,0)`),
											position: 'static',
										}}
									>
										<Card key={project.id}>
											<StyledRow>
												<div className="image-wrapper">
													{imageUrl ? (
														<ProjectImage
															imageUrl={project.image_url}
															projectUrl={project.url}
														/>
													) : (
														<Placeholder />
													)}
												</div>
												<div className="description">
													<TitleStyled projectName={project.name} projectUrl={project.url} />
													<CardBody>{project.description}</CardBody>
												</div>
											</StyledRow>
										</Card>
									</animated.div>
								)
							})}
						</Trail>
					</Container>
				</div>
			)}
		</Transition>
	)
}

export const portfolioPageQuery = graphql`
	query PortfolioPage($path: String!) {
		markdownRemark(frontmatter: { path: { eq: $path } }) {
			html
			frontmatter {
				path
				title
			}
		}
		site {
			siteMetadata {
				title
			}
		}
	}
`
