import React from 'react'
import { Label, Button, FormText, Container, Form, FormGroup, Input } from 'reactstrap'
import Helmet from 'react-helmet'
import graphql from 'graphql'
import { Transition } from 'react-spring'

export default function Template({ data }) {
	const { markdownRemark: post } = data
	return (
		<Transition force from={{ opacity: 0 }} enter={{ opacity: 1 }} leave={{ opacity: 0 }}>
			{(style) => (
				<div style={style}>
					<Helmet title={`${post.frontmatter.title} | ${data.site.siteMetadata.title}`} />
					<Container className="contact-container">
						<h1 className="display-4">{post.frontmatter.title}</h1>
						<Form method="POST" action="https://formspree.io/jon.walz.music@gmail.com">
							<FormGroup>
								<Input type="email" name="email" placeholder="Your email" />
							</FormGroup>
							<FormGroup>
								<Input type="textarea" name="body" placeholder="Send me a message!" />
							</FormGroup>
							<Button type="submit">Send</Button>
						</Form>
					</Container>
				</div>
			)}
		</Transition>
	)
}

export const contactPageQuery = graphql`
	query ContactPage($path: String!) {
		markdownRemark(frontmatter: { path: { eq: $path } }) {
			html
			frontmatter {
				path
				title
			}
		}
		site {
			siteMetadata {
				title
			}
		}
	}
`
